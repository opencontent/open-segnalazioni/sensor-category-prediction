# segnalazioni_trainer
## Descrizione
Questo componente espone un API che, data una segnalazione come richiesta, risponde con la predizione delle 3 micro e macrocategorie più probabili per quella segnalazione.