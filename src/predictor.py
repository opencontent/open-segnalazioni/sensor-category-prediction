from fastapi import FastAPI, status
from pydantic import BaseModel
from ordered_set import OrderedSet
from typing import List
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords 
from nltk.stem.porter import PorterStemmer
from autocorrect import Speller
from simplemma import text_lemmatizer
from joblib import load
from minio import Minio
from minio.error import InvalidResponseError
from minio.error import S3Error
from pprint import pformat
from uvicorn import Config, Server
from pythonjsonlogger import jsonlogger
from starlette_exporter import PrometheusMiddleware, handle_metrics

import os, os.path
import time
import pandas as pd
import numpy as np
import ast
import re
import nltk
import simplemma
import json
import pickle
import logging
import sklearn
import sys
import uvicorn

nltk.download('stopwords')
nltk.download('punkt')

HOST_URI = os.getenv('BUCKET_ENDPOINT_HOST_URI', default='localhost:9000')
BUCKET_NAME = os.getenv('BUCKET_NAME', default='opensegnalazioni-model-bucket')
ACCESS_KEY = os.getenv('BUCKET_ENDPOINT_ACCESS_KEY_ID', default='AKIAIOSFODNN7EXAMPLE')
SECRET_KEY = os.getenv('BUCKET_ENDPOINT_SECRET_ACCESS_KEY', default='wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY')
REGION = os.getenv('BUCKET_ENDPOINT_REGION', default=None)
CHILD_CAT_ENCODING = os.getenv('CHILD_CAT_ENCODING_NAME', default='cat_encoding_close.json')
PARENT_CAT_ENCODING = os.getenv('PARENT_CAT_ENCODING_NAME', default='parent_cat_encoding_close.json')
VECTORIZER_MICRO_CAT = os.getenv('VECTORIZER_MICRO_CAT_NAME', default='vectorizer_micro_cat.pk')
VECTORIZER_MACRO_CAT = os.getenv('VECTORIZER_MACRO_CAT_NAME', default='vectorizer_macro_cat.pk')
MODEL_MICRO_CAT = os.getenv('MODEL_MICRO_CAT_NAME', default='model_micro_cat.joblib')
MODEL_MACRO_CAT = os.getenv('MODEL_MACRO_CAT_NAME', default='model_macro_cat.joblib')
FASTAPI_APP_HOST = os.getenv('FASTAPI_APP_HOST', default='0.0.0.0')
FASTAPI_APP_PORT = os.getenv('FASTAPI_APP_PORT', default=5004)
PROMETHEUS_JOB_NAME = os.getenv('PROMETHEUS_JOB_NAME', default='category_predictor')

app = FastAPI()
top_k = 3
top_parent_k = 3

logging.info('Downloading prediction files...')
minioClient = Minio(HOST_URI, access_key=ACCESS_KEY, secret_key=SECRET_KEY, secure=True if "amazonaws" in HOST_URI else False, region=REGION)
minioClient.fget_object(BUCKET_NAME, MODEL_MICRO_CAT, MODEL_MICRO_CAT)
minioClient.fget_object(BUCKET_NAME, MODEL_MACRO_CAT, MODEL_MACRO_CAT)
minioClient.fget_object(BUCKET_NAME, VECTORIZER_MICRO_CAT, VECTORIZER_MICRO_CAT)
minioClient.fget_object(BUCKET_NAME, VECTORIZER_MACRO_CAT, VECTORIZER_MACRO_CAT)
minioClient.fget_object(BUCKET_NAME, CHILD_CAT_ENCODING, CHILD_CAT_ENCODING)
minioClient.fget_object(BUCKET_NAME, PARENT_CAT_ENCODING, PARENT_CAT_ENCODING)    

app.add_middleware(PrometheusMiddleware, app_name=PROMETHEUS_JOB_NAME, prefix=PROMETHEUS_JOB_NAME)

class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        message = log_record['message']
        message = message.replace("\"", "")
        message = message.split(" ")
        log_record['host'] = message[0]
        log_record['method'] = message[2]
        log_record['resource'] = message[3]
        log_record['http_version'] = message[4]
        log_record['status_code'] = message[5]
        log_record.pop('message', None)  

class Segnalazione(BaseModel):
    subject: str
    description: str

class ChildCategory(BaseModel):
    categoryId: int
    categoryName: str
    probability: float

class ChildPredCats(BaseModel):
    pred_cats: List[ChildCategory]

class ParentCategory(BaseModel):
    parentCategoryId: int
    parentCategoryName: str
    probability: float

class ParentPredCats(BaseModel):
    pred_cats: List[ParentCategory]

class HealthCheck(BaseModel):
    healthcheck: str
            
def init_data():
    stop_words_set = stopwords.words('italian')
    stemmer = PorterStemmer()
    spell = Speller(lang='it')
    langdata = simplemma.load_data('it')
    with open(os.getcwd() + '/src/data/ITGivenFemale.json') as f:
        data_female = json.load(f)
    with open(os.getcwd() + '/src/data/ITGivenMale.json') as f:
        data_male = json.load(f)

    data_names = list()
    for i in range(len(data_female)):
        data_names.append(str(data_female[i]['name']))
    for i in range(len(data_male)):
        data_names.append(str(data_male[i]['name']))
    for i in range(len(data_names)):
        data_names[i] = data_names[i].lower()
    
    df_cognomi = pd.read_csv(os.getcwd() + '/src/data/cognomi.txt')
    df_cognomi['Cognome'] = df_cognomi['Cognome'].str.lower()
    df_cognomi = list(df_cognomi['Cognome'])
    
    df_comuni = pd.read_csv(os.getcwd() + '/src/data/Elenco-comuni-italiani.csv', sep=';', encoding='latin1')
    df_comuni['Denominazione in italiano'] = df_comuni['Denominazione in italiano'].str.lower()
    df_comuni = list(df_comuni['Denominazione in italiano'])
    
    with open(os.getcwd() + '/src/data/vie_genova.json') as f:
        data_streets = json.load(f)
    df_streets = pd.DataFrame(data_streets['Risposta']['Strada'])
    df_streets['NOME_VIA'] = df_streets['NOME_VIA'].str.lower()

    corpus = ""
    for item in df_streets:
        corpus = corpus + " " + item
    corpus = OrderedSet(word_tokenize(corpus))
    corpus = list(corpus)
    df_streets = corpus

    return stop_words_set, stemmer, spell, langdata, data_names, df_comuni, df_cognomi, df_streets

logging.info("Loading datasets...")
stop_words_set, stemmer, spell, langdata, data_names, df_comuni, df_cognomi, df_streets = init_data()    
def clean_document(document):
    document = re.sub('[^A-Za-z]', ' ', document)
    document = document.lower()

    tokenized_text = word_tokenize(document)
    tokenized_text = [w for w in tokenized_text if not w in stop_words_set]
    tokenized_text = [w for w in tokenized_text if not w in data_names]
    tokenized_text = [w for w in tokenized_text if not w in df_comuni]
    tokenized_text = [w for w in tokenized_text if not w in df_cognomi]
    tokenized_text = [w for w in tokenized_text if not w in df_streets]
    tokenized_text = [w for w in tokenized_text if not w in ['copia']]
    
    words = []
    for word in tokenized_text:
        words.append(stemmer.stem(" ".join(text_lemmatizer(spell(word), langdata))))
    cleaned_text = " ".join(words)
    document = cleaned_text
    return document

def extract_features(text, filename):
    tfidf_vectorizer = pd.read_pickle(filename)
    data = pd.DataFrame({'subject_description':[text]})
    text_vectorized = tfidf_vectorizer.transform(data['subject_description'])
    return text_vectorized

def get_top_k_predictions(model,text,k):
    probs = model.predict_proba(text)
    best_n = np.argsort(probs, axis=1)[:,-k:]
    best_n_probs = np.sort(probs, axis=1)[:,-k:]
    preds=[[model.classes_[predicted_cat] for predicted_cat in prediction] for prediction in best_n]
    preds=[item[::-1] for item in preds]
    preds_probs=[item[::-1] for item in best_n_probs]
    return preds, preds_probs

def get_pred_cats(preds, preds_probs):
    pred_cats = pd.DataFrame(columns=['categoryId','categoryName', 'probability'])
    
    with open(CHILD_CAT_ENCODING) as f:
        cat_encoding = json.load(f)
    cat_encoding_df = pd.DataFrame(cat_encoding)
    
    for item in preds:
        for idx, cat in enumerate(item):
            pred_cats = pred_cats.append({
                'categoryId': int(cat_encoding_df['categoryId'][cat_encoding_df['label'] == cat]),
                'categoryName': cat_encoding_df['categoryName'][cat_encoding_df['label'] == cat].values[0],
                'probability': round(preds_probs[0][idx], 3)
            }, ignore_index=True)
    return pred_cats

def get_pred_parent_cats(preds, preds_probs):
    pred_cats = pd.DataFrame(columns=['parentCategoryId','parentCategoryName', 'probability'])
    
    with open(PARENT_CAT_ENCODING) as f:
        cat_encoding = json.load(f)
    cat_encoding_df = pd.DataFrame(cat_encoding)
    
    for item in preds:
        for idx, cat in enumerate(item):
            pred_cats = pred_cats.append({
                'parentCategoryId': int(cat_encoding_df['parentCategoryId'][cat_encoding_df['parent_label'] == cat].unique()),
                'parentCategoryName': cat_encoding_df['parentCategoryName'][cat_encoding_df['parent_label'] == cat].values[0],
                'probability': round(preds_probs[0][idx], 3)
            }, ignore_index=True)    
    return pred_cats

@app.post("/categoryhints/micro/", response_model=ChildPredCats)
async def predict_child_categories(segnalazione: Segnalazione):
    text = segnalazione.subject + " " + segnalazione.description
    text = clean_document(text)
    text = extract_features(text, VECTORIZER_MICRO_CAT)
    model = load(MODEL_MICRO_CAT)
    preds, preds_probs = get_top_k_predictions(model,text,top_k)
    pred_cats = get_pred_cats(preds, preds_probs)

    return {'pred_cats': pred_cats.to_dict(orient='records')}

@app.post("/categoryhints/macro/", response_model=ParentPredCats)
async def predict_parent_categories(segnalazione: Segnalazione):
    text = segnalazione.subject + " " + segnalazione.description
    text = clean_document(text)
    text = extract_features(text, VECTORIZER_MACRO_CAT)
    model = load(MODEL_MACRO_CAT)
    preds, preds_probs = get_top_k_predictions(model,text,top_parent_k)
    pred_cats = get_pred_parent_cats(preds, preds_probs)

    return {'pred_cats': pred_cats.to_dict(orient='records')}

@app.get('/healthcheck', response_model=HealthCheck, status_code=status.HTTP_200_OK)
def perform_healthcheck():
    if(not os.path.exists(MODEL_MICRO_CAT)):
        minioClient.fget_object(BUCKET_NAME, MODEL_MICRO_CAT, MODEL_MICRO_CAT) 
    if(time.time() - os.path.getctime(MODEL_MICRO_CAT) >= 86400):
        logging.info('Updating prediction files...')
        minioClient.fget_object(BUCKET_NAME, MODEL_MICRO_CAT, MODEL_MICRO_CAT)
        minioClient.fget_object(BUCKET_NAME, MODEL_MACRO_CAT, MODEL_MACRO_CAT)
        minioClient.fget_object(BUCKET_NAME, VECTORIZER_MICRO_CAT, VECTORIZER_MICRO_CAT)
        minioClient.fget_object(BUCKET_NAME, VECTORIZER_MACRO_CAT, VECTORIZER_MACRO_CAT)
        minioClient.fget_object(BUCKET_NAME, CHILD_CAT_ENCODING, CHILD_CAT_ENCODING)
        minioClient.fget_object(BUCKET_NAME, PARENT_CAT_ENCODING, PARENT_CAT_ENCODING)    
    
    return {'healthcheck': 'Everything OK!'}

@app.on_event('startup')
async def startup_event():
    logger = logging.getLogger('uvicorn.access')
    logHandler = logging.StreamHandler()
    formatter = CustomJsonFormatter('%(asctime)s %(message)s')
    logHandler.setFormatter(formatter)
    logger.addHandler(logHandler)

app.add_route("/metrics", handle_metrics)

if __name__ == "__main__":
    server = Server(Config("predictor:app", host=FASTAPI_APP_HOST, port=int(FASTAPI_APP_PORT), 
                            proxy_headers=True, forwarded_allow_ips='*', access_log=False))
    server.run()