# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster

ARG USERNAME=predictor
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME

# [Optional] Set the default user. Omit if you want to keep the default as root.
USER $USERNAME

EXPOSE 5004

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app

RUN mkdir src
RUN mkdir src/data

COPY /src/predictor.py /app/src 
COPY /src/data/ITGivenFemale.json /app/src/data
COPY /src/data/ITGivenMale.json /app/src/data
COPY /src/data/cognomi.txt /app/src/data
COPY /src/data/Elenco-comuni-italiani.csv /app/src/data
COPY /src/data/vie_genova.json /app/src/data

CMD ["python3", "src/predictor.py"]
